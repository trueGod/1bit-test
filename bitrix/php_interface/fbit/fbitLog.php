<?php
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("fbitLog", "OnAfterIBlockElementUpdateHandler"));

class fbitLog
{
    private static $conf = false;
    private static function getConfig($param = false)
    {
        if (!self::$conf)
            self::$conf = Bitrix\Main\Config\Configuration::getValue('fbitLog');
        if (!$param)
            return self::$conf;
        else
            return self::$conf[$param];
    }

    private function addToLog($var)
    {
        $addText = ConvertTimeStamp(false, 'FULL') . " | " . $var . "\n";
        $file = fopen(self::getConfig('log_dir'), "a");
        fwrite($file, $addText);
        fclose($file);
    }

    private function clearLog()
    {
        $file = fopen(self::getConfig('log_dir'), "w");
        fclose($file);
    }

    public function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] == self::getConfig('iblock_id')) {
            global $USER;
            $user = "[" . $USER->GetID() . "]" . $USER->GetFirstName() . " " . $USER->GetLastName() .
                " - изменил элемент инфоблока, ID элемента: " . $arFields['ID'];
            self::addToLog($user);
        }
    }

    public function sendLog()
    {
        $GLOBALS['USER'] = new CUser;
        if (CModule::IncludeModule("form")) {
            $logText = file_get_contents(self::getConfig('log_dir'));
            CFormResult::Add(self::getConfig('form_id'), array("form_textarea_1" => $logText), "N");

            #CFormResult::Mail($resultID);
            #Использует константу SITE_ID, использовать на хитах не получиться

            $arEventFields = array(
                "SIMPLE_QUESTION_576" => $logText,
            );

            CEvent::Send("FORM_FILLING_SIMPLE_FORM_1", "s1", $arEventFields);

            self::clearLog();
            return "fbitLog::sendLog();";
        }
    }
}