<?
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/fbit/fbitLog.php"))
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/fbit/fbitLog.php");

/*
 * Дано CITY. Необходимо с помощью API Битрикс сделать выборку всех курсов в известном нам городе в
 * расписании которых в поле начала активности дата больше или равна текущей.
 * Данную выборку необходимо обернуть в php кеш.
 */
function getCoursesByCityID($CITY_ID)
{
    $obCache = new CPHPCache();

    if ($obCache->InitCache(3600, $CITY_ID, '/getCoursesByCityID/' . $CITY_ID)) {
        $vars = $obCache->GetVars();
        return $vars;
    } elseif ($obCache->StartDataCache()) {
        if (CModule::IncludeModule("iblock")) {
            global $DB;
            $res = CIBlockElement::GetList(
                Array("ID" => "ASC"),
                Array(
                    "IBLOCK_ID" => 5,
                    "PROPERTY_CITY" => $CITY_ID,
                    "ID" =>
                        CIBlockElement::SubQuery("PROPERTY_COURSE_ID",
                            array(
                                "IBLOCK_ID" => 6,
                                ">=DATE_ACTIVE_FROM" => date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL"))),
                            )
                        )
                ),
                false,
                false,
                array(
                    'IBLOCK_ID',
                    'ID'
                )
            );

            $arResult = false;
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arProp = $ob->GetProperties();

                $arFields['CITY'] = $arProp['CITY']['VALUE'];
                $arResult[] = $arFields;
            }

            $obCache->EndDataCache($arResult);
            return $arResult;
        }
    }
}

